pragma solidity ^0.5.0;

contract SampleA {
    
    uint x;
    
    constructor() public {}

    function setX(uint _x) public {
        x = _x;
    }
    
    function getX() public view returns(uint) {
        return x;
    }
    
    function balance(address addr) public view returns(uint) {
        return addr.balance;
    }
    
    function transfer(address payable addr) public payable {
        addr.transfer(msg.value);
    }
}

contract SampleB {
    
    SampleA a;
    
    constructor(SampleA _a) public {
        a = _a;
    }
    
    function setX(uint _x) public {
        a.setX(_x);
    }
    
    function getX() public view returns(uint) {
        return a.getX();
    }
}