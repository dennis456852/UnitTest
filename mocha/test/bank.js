const assert = require('assert');
const fs = require('fs');
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');

describe('Bank Contract', function () {

    let bankContract;
    let accounts;

    before(function (done) {
        web3.eth.getAccounts()
            .then(function (result) {
                accounts = result;
            })
            .then(function () {
                let abi = JSON.parse(fs.readFileSync("./build/__contracts_Bank_sol_Bank.abi").toString());
                let bytecode = '0x' + fs.readFileSync("./build/__contracts_Bank_sol_Bank.bin").toString();
                let bank = new web3.eth.Contract(abi);
                return bank.deploy({
                    data: bytecode
                })
                    .send({
                        from: accounts[0],
                        gas: 3400000
                    })
            })
            .then(function (instance) {
                bankContract = instance;
                done();
            })
    })

    describe('deposit', function () {
        it('should deposit 3 ether', function (done) {
            bankContract.methods.deposit()
                .send({
                    from: accounts[0],
                    gas: 3400000,
                    value: web3.utils.toWei("3", "ether")
                })
                .then(function (receipt) {
                    return bankContract.methods.getBankBalance()
                        .call({
                            from: accounts[0]
                        })
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("3", "ether"));
                    done();
                })
        })
    })

    describe('withdraw', function () {
        it('should withdraw 1 ether', function (done) {
            bankContract.methods.withdraw(1)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return bankContract.methods.getBankBalance()
                        .call({
                            from: accounts[0]
                        })
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("2", "ether"));
                    done();
                })
        })
    })

    describe('transfer', function () {
        it('should transfer 1 ether', function (done) {
            bankContract.methods.transfer(accounts[1], 1)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return bankContract.methods.getBankBalance()
                        .call({
                            from: accounts[1]
                        })
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("1", "ether"));
                    done();
                })
        })
    })

    describe('mint', function () {
        it('should mint 3*10**18 coin', function (done) {
            bankContract.methods.mint(3)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return bankContract.methods.getCoinBalance()
                        .call({
                            from: accounts[0]
                        })
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("3", "ether"));
                    done();
                })
        })
    })

    describe('transferCoin', function () {
        it('should transfer 10**18 coin', function (done) {
            bankContract.methods.transferCoin(accounts[1], 1)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return bankContract.methods.getCoinBalance()
                        .call({
                            from: accounts[1]
                        })
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("1", "ether"));
                    done();
                })
        })
    })
})