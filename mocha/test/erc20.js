const assert = require('assert');
const fs = require('fs');
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');

describe('ERC20 Contract', function () {

    let tokenContract;
    let accounts;

    before(function (done) {
        web3.eth.getAccounts()
            .then(function (result) {
                accounts = result;
            })
            .then(function () {
                let abi = JSON.parse(fs.readFileSync("./build/__contracts_ERC20_sol_ERC20.abi").toString());
                let bytecode = '0x' + fs.readFileSync("./build/__contracts_ERC20_sol_ERC20.bin").toString();
                let token = new web3.eth.Contract(abi);
                return token.deploy({
                    data: bytecode,
                    arguments: ["SimpleToken", "SIM", 18, 10000]
                })
                    .send({
                        from: accounts[0],
                        gas: 3400000
                    })
            })
            .then(function (instance) {
                tokenContract = instance;
                done();
            })
    })

    describe('transfer', function () {
        it('should transfer 10 token to account 1', function (done) {
            tokenContract.methods.transfer(accounts[1], web3.utils.toWei("10", "ether"))
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return tokenContract.methods.balanceOf(accounts[1])
                        .call()
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("10", "ether"));
                    done();
                })
        })
    })

    describe('approve', function () {
        it('should approve 10 token from account 1 to account 0', function (done) {
            tokenContract.methods.approve(accounts[0], web3.utils.toWei("10", "ether"))
                .send({
                    from: accounts[1],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return tokenContract.methods.allowance(accounts[1], accounts[0])
                        .call()
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("10", "ether"));
                    done();
                })
        })
    })

    describe('transferFrom', function () {
        it('should transfer 10 token from account 1 to account 2', function (done) {
            tokenContract.methods.transferFrom(accounts[1], accounts[2], web3.utils.toWei("10", "ether"))
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return tokenContract.methods.balanceOf(accounts[2])
                        .call()
                })
                .then(function (result) {
                    assert.equal(result, web3.utils.toWei("10", "ether"));
                    done();
                })
        })
    })

})