const assert = require('assert');
const fs = require('fs');
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');

describe('Sample Contract', function () {

    let sampleContractA;
    let sampleContractB;
    let accounts;

    before(function (done) {
        web3.eth.getAccounts()
            .then(function (result) {
                accounts = result;
            })
            .then(function(){
                let abi = JSON.parse(fs.readFileSync("./build/__contracts_Sample_sol_SampleA.abi").toString());
                let bytecode = '0x' + fs.readFileSync("./build/__contracts_Sample_sol_SampleA.bin").toString();
                let sample = new web3.eth.Contract(abi);
                return sample.deploy({
                        data: bytecode
                    })
                    .send({
                        from: accounts[0],
                        gas: 3400000
                    })
            })
            .then(function (instance) {
                sampleContractA = instance;
            })
            .then(function() {
                let abi = JSON.parse(fs.readFileSync("./build/__contracts_Sample_sol_SampleB.abi").toString());
                let bytecode = '0x' + fs.readFileSync("./build/__contracts_Sample_sol_SampleB.bin").toString();
                let sample = new web3.eth.Contract(abi);
                return sample.deploy({
                        data: bytecode,
                        arguments: [sampleContractA.options.address]
                    })
                    .send({
                        from: accounts[0],
                        gas: 3400000
                    })
            })
            .then(function (instance) {
                sampleContractB = instance;
                done();
            })
    })

    describe('setX', function () {
        it('should use contract SampleA to set x to 10', function (done) {
            sampleContractA.methods.setX(10)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return sampleContractA.methods.getX()
                        .call()
                })
                .then(function (result) {
                    assert.equal(result, 10);
                    done();
                })
        })
        it('should use contract SampleB to set x to 20', function (done) {
            sampleContractB.methods.setX(20)
                .send({
                    from: accounts[0],
                    gas: 3400000
                })
                .then(function (receipt) {
                    return sampleContractB.methods.getX()
                        .call()
                })
                .then(function (result) {
                    assert.equal(result, 20);
                    done();
                })
        })
    })

    describe('transfer', function () {
        it('should transfer 1 ether to account 1', function (done) {
            let balanceBefore
            sampleContractA.methods.balance(accounts[1])
                .call()
                .then(function (result) {
                    balanceBefore = result;
                })
                .then(function () {
                    return sampleContractA.methods.transfer(accounts[1])
                        .send({
                            from: accounts[0],
                            gas: 340000,
                            value: web3.utils.toWei("1", "ether")
                        })

                })
                .then(function (receipt) {
                    return sampleContractA.methods.balance(accounts[1])
                        .call()
                })
                .then(function (balanceAfter) {
                    assert.equal(balanceAfter - balanceBefore, web3.utils.toWei("1", "ether"));
                    done();
                })
        })
    })
})