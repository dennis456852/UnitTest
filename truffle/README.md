# UnitTest

## 方法一
## 1. 開啟 ganache
    ganache-cli
- 如果port有更改，需要修改truffle-config.js。

## 2. 單元測試
    truffle test
- ERC20#2會失敗，先前的合約會影響到後來的合約。
----

## 方法二
## 1. Truffle Develop
    truffle develop

## 2. 單元測試
    truffle(develop)> test