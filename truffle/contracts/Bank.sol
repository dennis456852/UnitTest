pragma solidity ^0.5.0;

contract Bank {
	// 此合約的擁有者
    address payable private owner;

	// 儲存所有會員的餘額
    mapping (address => uint256) private balance;

	// 儲存所有會員的coin餘額
    mapping (address => uint256) private coinBalance;

    // 儲存所有會員的定存
    mapping (address => uint256) private timeDeposit;

    // 儲存所有會員的定存期限
    mapping (address => uint256) private timeDepositPeriod;

	// 事件們，用於通知前端 web3.js
    event DepositEvent(address indexed from, uint256 value, uint256 timestamp);
    event WithdrawEvent(address indexed from, uint256 value, uint256 timestamp);
    event TransferEvent(address indexed from, address indexed to, uint256 value, uint256 timestamp);

    event MintEvent(address indexed from, uint256 value, uint256 timestamp);
    event BuyCoinEvent(address indexed from, uint256 value, uint256 timestamp);
    event TransferCoinEvent(address indexed from, address indexed to, uint256 value, uint256 timestamp);
    event TransferOwnerEvent(address indexed oldOwner, address indexed newOwner, uint256 timestamp);

    event StartTimeDepositEvent(address indexed from, uint256 value, uint256 period, uint256 timestamp);
    event CompleteTimeDepositEvent(address indexed from, uint256 money, uint256 interest, uint256 timestamp);
    event CancelTimeDepositEvent(address indexed from, uint256 money, uint256 interest, uint256 timestamp);

    modifier isOwner() {
        require(owner == msg.sender, "you are not owner");
        _;
    }

	// 建構子
    constructor() public {
        owner = msg.sender;
    }

	// 存錢
    function deposit() public payable {
        balance[msg.sender] += msg.value;

        emit DepositEvent(msg.sender, msg.value, now);
    }

	// 提錢
    function withdraw(uint256 etherValue) public {
        uint256 weiValue = etherValue * 1 ether;

        require(balance[msg.sender] >= weiValue, "your balances are not enough");

        msg.sender.transfer(weiValue);

        balance[msg.sender] -= weiValue;

        emit WithdrawEvent(msg.sender, etherValue, now);
    }

	// 轉帳
    function transfer(address to, uint256 etherValue) public {
        uint256 weiValue = etherValue * 1 ether;

        require(balance[msg.sender] >= weiValue, "your balances are not enough");

        balance[msg.sender] -= weiValue;
        balance[to] += weiValue;

        emit TransferEvent(msg.sender, to, etherValue, now);
    }

	// mint coin
    function mint(uint256 coinValue) public isOwner {
        uint256 value = coinValue * 1 ether;

        coinBalance[msg.sender] += value;

        emit MintEvent(msg.sender, coinValue, now);
    }

	// buy coin
    function buy(uint256 coinValue) public {
        uint256 value = coinValue * 1 ether;

        require(coinBalance[owner] >= value, "owner's coin balances are not enough");
        require(balance[msg.sender] >= value, "your balances are not enough");

        balance[msg.sender] -= value;
        balance[owner] += value;

        coinBalance[msg.sender] += value;
        coinBalance[owner] -= value;

        emit BuyCoinEvent(msg.sender, coinValue, now);
    }

	// 轉 coin
    function transferCoin(address to, uint256 coinValue) public {
        uint256 value = coinValue * 1 ether;

        require(coinBalance[msg.sender] >= value, "your coin balances are not enough");

        coinBalance[msg.sender] -= value;
        coinBalance[to] += value;

        emit TransferCoinEvent(msg.sender, to, coinValue, now);
    }

	// 檢查銀行帳戶餘額
    function getBankBalance() public view returns (uint256) {
        return balance[msg.sender];
    }

    // 檢查coin餘額
    function getCoinBalance() public view returns (uint256) {
        return coinBalance[msg.sender];
    }

    // get owner
    function getOwner() public view returns (address)  {
        return owner;
    }

    // 轉移owner
    function transferOwner(address payable newOwner) public isOwner {
        address oldOwner = owner;
        owner = newOwner;
        emit TransferOwnerEvent(oldOwner, newOwner, now);
    }

    // 開始定存
    function startTimeDeposit(uint256 coinValue, uint256 period) public {

        require(coinBalance[msg.sender] >= coinValue, "your coin balances are not enough");
        require(timeDeposit[msg.sender] == 0, "you only can start one time deposit");

        coinBalance[msg.sender] -= coinValue;
        timeDeposit[msg.sender] += coinValue;
        timeDepositPeriod[msg.sender] = period;

        emit StartTimeDepositEvent(msg.sender, coinValue, period, now);
    }
    // 結束定存
    function completeTimeDeposit() public {

        require(timeDeposit[msg.sender] > 0, "you do not have time deposit");

        uint256 money = timeDeposit[msg.sender];
        uint256 interestMoney = (timeDepositPeriod[msg.sender] * money) / 100;

        coinBalance[msg.sender] = coinBalance[msg.sender] + money + interestMoney;
        timeDeposit[msg.sender] = 0;
        timeDepositPeriod[msg.sender] = 0;

        emit CompleteTimeDepositEvent(msg.sender, money, money + interestMoney, now);
    }
    // 取消定存
    function cancelTimeDeposit(uint256 currentPeriod) public {

        require(timeDeposit[msg.sender] > 0, "you do not have time deposit");

        uint256 money = timeDeposit[msg.sender];
        uint256 interestMoney = (money * currentPeriod / 100);

        coinBalance[msg.sender] = coinBalance[msg.sender] + money + interestMoney;
        timeDeposit[msg.sender] -= money;
        timeDepositPeriod[msg.sender] = 0;

        emit CancelTimeDepositEvent(msg.sender, money, money + interestMoney, now);
    }

    // 取得定存
    function getTimeDeposit() public view returns(uint256, uint256) {
        return (timeDeposit[msg.sender], timeDepositPeriod[msg.sender]);
    }

    function kill() public isOwner {
        selfdestruct(owner);
    }
}