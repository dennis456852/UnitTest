var bank = artifacts.require("Bank");
var token = artifacts.require("ERC20");
var sampleA = artifacts.require("SampleA");
var sampleB = artifacts.require("SampleB");

module.exports = function (deployer) {
  deployer.deploy(bank);
  deployer.deploy(token, "SimpleToken", "SIM", 18, 10000);
  deployer.deploy(sampleA).then(function () {
    return deployer.deploy(sampleB, sampleA.address);
  });
};
