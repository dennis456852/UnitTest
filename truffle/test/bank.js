const bank = artifacts.require('Bank');

contract('Bank', accounts => {
    it('should deposit 3 ether', async () => {
        let bankContract = await bank.deployed()
        await bankContract.deposit({
            from: accounts[0],
            value: web3.utils.toWei("3", "ether")
        })
        let balance = await bankContract.getBankBalance.call({
            from: accounts[0]
        })
        assert.equal(balance.toString(), web3.utils.toWei("3", "ether"), 'deposit error')
    })
    it('should withdraw 1 ether', async () => {
        let bankContract = await bank.deployed()
        await bankContract.withdraw(1, {
            from: accounts[0]
        })
        let balance = await bankContract.getBankBalance.call({
            from: accounts[0]
        })
        assert.equal(balance.toString(), web3.utils.toWei("2", "ether"), 'withdraw error')
    })
    it('should transfer 1 ether', async () => {
        let bankContract = await bank.deployed()
        await bankContract.transfer(accounts[1], 1, {
            from: accounts[0]
        })
        let balance = await bankContract.getBankBalance.call({
            from: accounts[1]
        })
        assert.equal(balance.toString(), web3.utils.toWei("1", "ether"), 'transfer error')
    })
    it('should mint 3*10**18 coin', async () => {
        let bankContract = await bank.deployed()
        await bankContract.mint(3, {
            from: accounts[0]
        })
        let balance = await bankContract.getCoinBalance.call({
            from: accounts[0]
        })
        assert.equal(balance.toString(), web3.utils.toWei("3", "ether"), 'mint error')
    })
    it('should transfer 10**18 coin', async () => {
        let bankContract = await bank.deployed()
        await bankContract.transferCoin(accounts[1], 1, {
            from: accounts[0]
        })
        let balance = await bankContract.getCoinBalance.call({
            from: accounts[1]
        })
        assert.equal(balance.toString(), web3.utils.toWei("1", "ether"), 'mint error')
    })
})