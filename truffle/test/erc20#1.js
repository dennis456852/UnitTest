const token = artifacts.require('ERC20');

contract('ERC20#1', accounts => {
    it('should transfer 10 token to account 1', async () => {
        let tokenContract = await token.deployed()
        await tokenContract.transfer(accounts[1], web3.utils.toWei("10", "ether"), {
            from: accounts[0]
        })
        let balance = await tokenContract.balanceOf.call(accounts[1])
        assert.equal(balance.toString(), web3.utils.toWei("10", "ether"), 'transfer error')
    })
    it('should approve 10 token from account 1 to account 0', async () => {
        let tokenContract = await token.deployed()
        await tokenContract.approve(accounts[0], web3.utils.toWei("10", "ether"), {
            from: accounts[1]
        })
        let allowance = await tokenContract.allowance.call(accounts[1], accounts[0])
        assert.equal(allowance.toString(), web3.utils.toWei("10", "ether"), 'approve error')
    })
    it('should transfer 10 token from account 1 to account 2', async () => {
        let tokenContract = await token.deployed()
        await tokenContract.transferFrom(accounts[1], accounts[2], web3.utils.toWei("10", "ether"), {
            from: accounts[0]
        })
        let balance = await tokenContract.balanceOf.call(accounts[2])
        assert.equal(balance.toString(), web3.utils.toWei("10", "ether"), 'transfer error')
    })
})