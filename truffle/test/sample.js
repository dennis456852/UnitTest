const sampleA = artifacts.require('SampleA');
const sampleB = artifacts.require('SampleB');

contract('Sample', accounts => {
    it('should use contract SampleA to set x to 10', async () => {
        let sampleContract = await sampleA.deployed()
        await sampleContract.setX(10, {
            from: accounts[0]
        })
        let value = await sampleContract.getX.call()
        assert.equal(value.toNumber(), 10, 'SampleA setX error')
    })
    it('should use contract sampleB to set x to 20', async () => {
        let sampleContract = await sampleB.deployed()
        await sampleContract.setX(20, {
            from: accounts[0]
        })
        let value = await sampleContract.getX.call()
        assert.equal(value.toNumber(), 20, 'sampleB setX error')
    })
    it('should transfer 1 ether to account 1', async () => {
        let sampleContract = await sampleA.deployed()
        let balanceBefore = await sampleContract.balance.call(accounts[1])  // or web3.eth.getBalance(accounts[1]))
        await sampleContract.transfer(accounts[1], {
            from: accounts[0],
            value: web3.utils.toWei('1', 'ether')
        })
        let balanceAfter = await sampleContract.balance.call(accounts[1])   // or web3.eth.getBalance(accounts[1]))
        assert.equal(balanceAfter - balanceBefore, web3.utils.toWei('1', 'ether'), 'account 0 does not transfer 1 ether to account 1')
    })
})